# FPV Bot 2.1 - Chassis Upgrade

This is a drop-in replacement for the acrylic chassis shown in the bot-chassis-2.0. 

Now that a lot of the electronics are performing as specified, it is time to freeze elements of that design  and fabricate a simple housing to help protect the longevity of the robot.

*I've switched to Fusion 360 instead of SolidWorks as the tool is more streamlined for small, early stage prototyping, and more affordable license. The design timeline really improves the work flow of every design.*

## Upgrade Plan

Please click on the  video below to watch my plan to move forward with the chassis update.

[![Bot Chassis Update](http://img.youtube.com/vi/1rS0-2rwe08/0.jpg)](http://www.youtube.com/watch?v=1rS0-2rwe08 "Upgrade Plan")

Some clients seem to prefer video updates rather than the wall of text that can happen when explaining a design from the ground up in an email.



### Motor Mount

![Motor Mount](motor-mount.png)

### Camera Mount

![Camera Mount](camera-mount.png)

### Enclosure

![Bot Enclosure](enclosure.png)

